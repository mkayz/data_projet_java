package org.mohamed.testdata.Controller;

import org.mohamed.testdata.Entity.UserEntity;
import org.mohamed.testdata.Repository.UserRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    private final UserRepository userRepository;

    public HomeController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/home")
    public String Home()
    {

        return "home";
    }

    @GetMapping("/allUsers")
    Iterable<UserEntity> getAllUser()
    {
        return userRepository.findAll();
    }
}
