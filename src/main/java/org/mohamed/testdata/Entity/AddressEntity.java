package org.mohamed.testdata.Entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "address", schema = "onlinebet")
public class AddressEntity {
    private Integer id;
    private String addressNumberAndStreet;
    private int zipCode;
    private String city;
    private String country;

    @Id
    @GeneratedValue
    @Column(name = "id")
    public Integer getId() {
        return id;
    }


    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "address_number_and_street")
    public String getAddressNumberAndStreet() {
        return addressNumberAndStreet;
    }

    public void setAddressNumberAndStreet(String addressNumberAndStreet) {
        this.addressNumberAndStreet = addressNumberAndStreet;
    }

    @Basic
    @Column(name = "zip_code")
    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    @Basic
    @Column(name = "city")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Basic
    @Column(name = "country")
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddressEntity that = (AddressEntity) o;
        return zipCode == that.zipCode && Objects.equals(id, that.id) && Objects.equals(addressNumberAndStreet, that.addressNumberAndStreet) && Objects.equals(city, that.city) && Objects.equals(country, that.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, addressNumberAndStreet, zipCode, city, country);
    }
}
