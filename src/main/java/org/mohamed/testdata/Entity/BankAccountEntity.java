package org.mohamed.testdata.Entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "bank_account", schema = "onlinebet")
public class BankAccountEntity {
    private Long id;
    private String ibanCode;
    private String bicCode;

    @Id
    @GeneratedValue
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "iban_code")
    public String getIbanCode() {
        return ibanCode;
    }

    public void setIbanCode(String ibanCode) {
        this.ibanCode = ibanCode;
    }

    @Basic
    @Column(name = "bic_code")
    public String getBicCode() {
        return bicCode;
    }

    public void setBicCode(String bicCode) {
        this.bicCode = bicCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankAccountEntity that = (BankAccountEntity) o;
        return Objects.equals(id, that.id) && Objects.equals(ibanCode, that.ibanCode) && Objects.equals(bicCode, that.bicCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ibanCode, bicCode);
    }
}
