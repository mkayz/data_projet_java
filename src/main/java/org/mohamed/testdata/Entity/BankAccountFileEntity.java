package org.mohamed.testdata.Entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "bank_account_file", schema = "onlinebet")
public class BankAccountFileEntity {
    private Long id;
    private String name;
    private byte valid;

    @Id
    @GeneratedValue
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "valid")
    public byte getValid() {
        return valid;
    }

    public void setValid(byte valid) {
        this.valid = valid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankAccountFileEntity that = (BankAccountFileEntity) o;
        return valid == that.valid && Objects.equals(id, that.id) && Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, valid);
    }
}
