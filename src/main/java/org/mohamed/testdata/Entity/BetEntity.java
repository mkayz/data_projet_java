package org.mohamed.testdata.Entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bet", schema = "onlinebet")
public class BetEntity {
    private Long id;
    private Timestamp betLimitTime;
    private String listOfOdds;
    private byte betOpened;
    private String betResult;

    @Id
    @GeneratedValue
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "bet_limit_time")
    public Timestamp getBetLimitTime() {
        return betLimitTime;
    }

    public void setBetLimitTime(Timestamp betLimitTime) {
        this.betLimitTime = betLimitTime;
    }

    @Basic
    @Column(name = "list_of_odds")
    public String getListOfOdds() {
        return listOfOdds;
    }

    public void setListOfOdds(String listOfOdds) {
        this.listOfOdds = listOfOdds;
    }

    @Basic
    @Column(name = "bet_opened")
    public byte getBetOpened() {
        return betOpened;
    }

    public void setBetOpened(byte betOpened) {
        this.betOpened = betOpened;
    }

    @Basic
    @Column(name = "bet_result")
    public String getBetResult() {
        return betResult;
    }

    public void setBetResult(String betResult) {
        this.betResult = betResult;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BetEntity betEntity = (BetEntity) o;
        return betOpened == betEntity.betOpened && Objects.equals(id, betEntity.id) && Objects.equals(betLimitTime, betEntity.betLimitTime) && Objects.equals(listOfOdds, betEntity.listOfOdds) && Objects.equals(betResult, betEntity.betResult);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, betLimitTime, listOfOdds, betOpened, betResult);
    }
}
