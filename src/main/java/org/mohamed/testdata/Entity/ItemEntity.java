package org.mohamed.testdata.Entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "item", schema = "onlinebet")
public class ItemEntity {
    private Long id;
    private int expectedBetResult;
    private int recordedOdds;
    private int amount;
    private int itemStatusId;

    @Id
    @GeneratedValue
    @Column(name = "id")
    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "expected_bet_result")
    public int getExpectedBetResult() {
        return expectedBetResult;
    }

    public void setExpectedBetResult(int expectedBetResult) {
        this.expectedBetResult = expectedBetResult;
    }

    @Basic
    @Column(name = "recorded_odds")
    public int getRecordedOdds() {
        return recordedOdds;
    }

    public void setRecordedOdds(int recordedOdds) {
        this.recordedOdds = recordedOdds;
    }

    @Basic
    @Column(name = "amount")
    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Basic
    @Column(name = "item_status_id")
    public int getItemStatusId() {
        return itemStatusId;
    }

    public void setItemStatusId(int itemStatusId) {
        this.itemStatusId = itemStatusId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ItemEntity that = (ItemEntity) o;
        return expectedBetResult == that.expectedBetResult && recordedOdds == that.recordedOdds && amount == that.amount && itemStatusId == that.itemStatusId && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, expectedBetResult, recordedOdds, amount, itemStatusId);
    }
}
