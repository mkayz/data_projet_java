package org.mohamed.testdata.Entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "payment", schema = "onlinebet")
public class PaymentEntity {
    private Long id;
    private String paymentName;
    private Timestamp datePayment;
    private int sum;
    private int paymentStatusId;

    @Id
    @GeneratedValue
    @Column(name = "id")
    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "payment_name")
    public String getPaymentName() {
        return paymentName;
    }

    public void setPaymentName(String paymentName) {
        this.paymentName = paymentName;
    }

    @Basic
    @Column(name = "date_payment")
    public Timestamp getDatePayment() {
        return datePayment;
    }

    public void setDatePayment(Timestamp datePayment) {
        this.datePayment = datePayment;
    }

    @Basic
    @Column(name = "sum")
    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    @Basic
    @Column(name = "payment_status_id")
    public int getPaymentStatusId() {
        return paymentStatusId;
    }

    public void setPaymentStatusId(int paymentStatusId) {
        this.paymentStatusId = paymentStatusId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PaymentEntity that = (PaymentEntity) o;
        return sum == that.sum && paymentStatusId == that.paymentStatusId && Objects.equals(id, that.id) && Objects.equals(paymentName, that.paymentName) && Objects.equals(datePayment, that.datePayment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, paymentName, datePayment, sum, paymentStatusId);
    }
}
