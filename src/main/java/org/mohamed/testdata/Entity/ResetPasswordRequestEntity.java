package org.mohamed.testdata.Entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "reset_password_request", schema = "onlinebet")
public class ResetPasswordRequestEntity {
    private Long id;
    private String selector;
    private String hashedToken;
    private Timestamp requestedAt;
    private Timestamp expiresAt;

    @Id
    @GeneratedValue
    @Column(name = "id")
    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "selector")
    public String getSelector() {
        return selector;
    }

    public void setSelector(String selector) {
        this.selector = selector;
    }

    @Basic
    @Column(name = "hashed_token")
    public String getHashedToken() {
        return hashedToken;
    }

    public void setHashedToken(String hashedToken) {
        this.hashedToken = hashedToken;
    }

    @Basic
    @Column(name = "requested_at")
    public Timestamp getRequestedAt() {
        return requestedAt;
    }

    public void setRequestedAt(Timestamp requestedAt) {
        this.requestedAt = requestedAt;
    }

    @Basic
    @Column(name = "expires_at")
    public Timestamp getExpiresAt() {
        return expiresAt;
    }

    public void setExpiresAt(Timestamp expiresAt) {
        this.expiresAt = expiresAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ResetPasswordRequestEntity that = (ResetPasswordRequestEntity) o;
        return Objects.equals(id, that.id) && Objects.equals(selector, that.selector) && Objects.equals(hashedToken, that.hashedToken) && Objects.equals(requestedAt, that.requestedAt) && Objects.equals(expiresAt, that.expiresAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, selector, hashedToken, requestedAt, expiresAt);
    }
}
