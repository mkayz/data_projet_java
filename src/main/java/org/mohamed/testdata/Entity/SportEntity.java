package org.mohamed.testdata.Entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "sport", schema = "onlinebet")
public class SportEntity {
    private Long id;
    private String name;
    private int nbOfTeams;
    private int nbOfPlayers;
    private int nbOfSubstitutePlayers;

    @Id
    @GeneratedValue
    @Column(name = "id")
    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "nb_of_teams")
    public int getNbOfTeams() {
        return nbOfTeams;
    }

    public void setNbOfTeams(int nbOfTeams) {
        this.nbOfTeams = nbOfTeams;
    }

    @Basic
    @Column(name = "nb_of_players")
    public int getNbOfPlayers() {
        return nbOfPlayers;
    }

    public void setNbOfPlayers(int nbOfPlayers) {
        this.nbOfPlayers = nbOfPlayers;
    }

    @Basic
    @Column(name = "nb_of_substitute_players")
    public int getNbOfSubstitutePlayers() {
        return nbOfSubstitutePlayers;
    }

    public void setNbOfSubstitutePlayers(int nbOfSubstitutePlayers) {
        this.nbOfSubstitutePlayers = nbOfSubstitutePlayers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SportEntity that = (SportEntity) o;
        return nbOfTeams == that.nbOfTeams && nbOfPlayers == that.nbOfPlayers && nbOfSubstitutePlayers == that.nbOfSubstitutePlayers && Objects.equals(id, that.id) && Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, nbOfTeams, nbOfPlayers, nbOfSubstitutePlayers);
    }
}
