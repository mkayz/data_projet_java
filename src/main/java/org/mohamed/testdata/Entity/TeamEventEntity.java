package org.mohamed.testdata.Entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "team_event", schema = "onlinebet")
@IdClass(TeamEventEntityPK.class)
public class TeamEventEntity {
    private Long id;
    private int teamId;
    private int eventId;

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @Column(name = "team_id")
    public int getTeamId() {
        return teamId;
    }

    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }

    @Id
    @Column(name = "event_id")
    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TeamEventEntity that = (TeamEventEntity) o;
        return teamId == that.teamId && eventId == that.eventId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(teamId, eventId);
    }
}
