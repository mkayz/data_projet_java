package org.mohamed.testdata.Entity;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class TeamEventEntityPK implements Serializable {
    private int teamId;
    private int eventId;

    @Column(name = "team_id")
    @Id
    public int getTeamId() {
        return teamId;
    }

    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }

    @Column(name = "event_id")
    @Id
    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TeamEventEntityPK that = (TeamEventEntityPK) o;
        return teamId == that.teamId && eventId == that.eventId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(teamId, eventId);
    }
}
