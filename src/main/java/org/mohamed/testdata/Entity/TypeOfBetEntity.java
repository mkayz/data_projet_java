package org.mohamed.testdata.Entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "type_of_bet", schema = "onlinebet")
public class TypeOfBetEntity {
    private Long id;
    private String betType;

    @Id
    @GeneratedValue
    @Column(name = "id")
    public Long getId() {
        return id;
    }



    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "bet_type")
    public String getBetType() {
        return betType;
    }

    public void setBetType(String betType) {
        this.betType = betType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypeOfBetEntity that = (TypeOfBetEntity) o;
        return Objects.equals(id, that.id) && Objects.equals(betType, that.betType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, betType);
    }
}
