package org.mohamed.testdata.Entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "type_of_payment", schema = "onlinebet")
public class TypeOfPaymentEntity {
    private Long id;
    private String typeOfPayment;

    @Id
    @GeneratedValue
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "type_of_payment")
    public String getTypeOfPayment() {
        return typeOfPayment;
    }

    public void setTypeOfPayment(String typeOfPayment) {
        this.typeOfPayment = typeOfPayment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypeOfPaymentEntity that = (TypeOfPaymentEntity) o;
        return Objects.equals(id, that.id) && Objects.equals(typeOfPayment, that.typeOfPayment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, typeOfPayment);
    }
}
