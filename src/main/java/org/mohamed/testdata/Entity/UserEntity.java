package org.mohamed.testdata.Entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "user", schema = "onlinebet")
public class UserEntity {
    private Long id;
    private String lastName;
    private String firstName;
    private String email;
    private String password;
    private String roles;
    private Timestamp birthDate;
    private Timestamp createAt;
    private byte active;
    private Timestamp activeAt;
    private byte suspended;
    private Timestamp endSuspendAt;
    private byte deleted;
    private Timestamp deletedAt;

    @Id
    @GeneratedValue
    @Column(name = "id")
    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "last_name")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Basic
    @Column(name = "first_name")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "roles")
    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    @Basic
    @Column(name = "birth_date")
    public Timestamp getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Timestamp birthDate) {
        this.birthDate = birthDate;
    }

    @Basic
    @Column(name = "create_at")
    public Timestamp getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Timestamp createAt) {
        this.createAt = createAt;
    }

    @Basic
    @Column(name = "active")
    public byte getActive() {
        return active;
    }

    public void setActive(byte active) {
        this.active = active;
    }

    @Basic
    @Column(name = "active_at")
    public Timestamp getActiveAt() {
        return activeAt;
    }

    public void setActiveAt(Timestamp activeAt) {
        this.activeAt = activeAt;
    }

    @Basic
    @Column(name = "suspended")
    public byte getSuspended() {
        return suspended;
    }

    public void setSuspended(byte suspended) {
        this.suspended = suspended;
    }

    @Basic
    @Column(name = "end_suspend_at")
    public Timestamp getEndSuspendAt() {
        return endSuspendAt;
    }

    public void setEndSuspendAt(Timestamp endSuspendAt) {
        this.endSuspendAt = endSuspendAt;
    }

    @Basic
    @Column(name = "deleted")
    public byte getDeleted() {
        return deleted;
    }

    public void setDeleted(byte deleted) {
        this.deleted = deleted;
    }

    @Basic
    @Column(name = "deleted_at")
    public Timestamp getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Timestamp deletedAt) {
        this.deletedAt = deletedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserEntity that = (UserEntity) o;
        return active == that.active && suspended == that.suspended && deleted == that.deleted && Objects.equals(id, that.id) && Objects.equals(lastName, that.lastName) && Objects.equals(firstName, that.firstName) && Objects.equals(email, that.email) && Objects.equals(password, that.password) && Objects.equals(roles, that.roles) && Objects.equals(birthDate, that.birthDate) && Objects.equals(createAt, that.createAt) && Objects.equals(activeAt, that.activeAt) && Objects.equals(endSuspendAt, that.endSuspendAt) && Objects.equals(deletedAt, that.deletedAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, lastName, firstName, email, password, roles, birthDate, createAt, active, activeAt, suspended, endSuspendAt, deleted, deletedAt);
    }
}
