package org.mohamed.testdata.Entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "wallet", schema = "onlinebet")
public class WalletEntity {
    private Long id;
    private int balance;
    private int limitAmountPerWeek;
    private byte realMoney;

    @Id
    @GeneratedValue
    @Column(name = "id")
    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "balance")
    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    @Basic
    @Column(name = "limit_amount_per_week")
    public int getLimitAmountPerWeek() {
        return limitAmountPerWeek;
    }

    public void setLimitAmountPerWeek(int limitAmountPerWeek) {
        this.limitAmountPerWeek = limitAmountPerWeek;
    }

    @Basic
    @Column(name = "real_money")
    public byte getRealMoney() {
        return realMoney;
    }

    public void setRealMoney(byte realMoney) {
        this.realMoney = realMoney;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WalletEntity that = (WalletEntity) o;
        return balance == that.balance && limitAmountPerWeek == that.limitAmountPerWeek && realMoney == that.realMoney && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, balance, limitAmountPerWeek, realMoney);
    }
}
