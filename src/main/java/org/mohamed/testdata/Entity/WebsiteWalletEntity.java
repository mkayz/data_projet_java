package org.mohamed.testdata.Entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "website_wallet", schema = "onlinebet")
public class WebsiteWalletEntity {
    private Long id;
    private int balance;

    @Id
    @GeneratedValue
    @Column(name = "id")
    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "balance")
    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WebsiteWalletEntity that = (WebsiteWalletEntity) o;
        return balance == that.balance && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, balance);
    }
}
