package org.mohamed.testdata.Repository;

import org.mohamed.testdata.Entity.AddressEntity;
import org.springframework.data.repository.CrudRepository;

public interface AddressRepository extends CrudRepository<AddressEntity, Integer> {
}
