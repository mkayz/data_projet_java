package org.mohamed.testdata.Repository;

import org.mohamed.testdata.Entity.BankAccountFileEntity;
import org.springframework.data.repository.CrudRepository;

public interface BankAccountFileRepository extends CrudRepository<BankAccountFileEntity, Integer> {
}
