package org.mohamed.testdata.Repository;

import org.mohamed.testdata.Entity.BankAccountEntity;
import org.springframework.data.repository.CrudRepository;

// Integer je ne sais pas d'où ça vient poser la question à Michael
public interface BankAccountRepository extends CrudRepository<BankAccountEntity, Integer> {
}
