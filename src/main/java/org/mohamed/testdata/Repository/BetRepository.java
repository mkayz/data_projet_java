package org.mohamed.testdata.Repository;

import org.mohamed.testdata.Entity.BetEntity;
import org.springframework.data.repository.CrudRepository;

public interface BetRepository extends CrudRepository<BetEntity, Integer> {
}
