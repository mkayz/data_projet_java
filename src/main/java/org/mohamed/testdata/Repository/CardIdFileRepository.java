package org.mohamed.testdata.Repository;

import org.mohamed.testdata.Entity.CardIdFileEntity;
import org.springframework.data.repository.CrudRepository;

public interface CardIdFileRepository extends CrudRepository<CardIdFileEntity, Integer> {
}
