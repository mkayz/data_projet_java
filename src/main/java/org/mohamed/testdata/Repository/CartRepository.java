package org.mohamed.testdata.Repository;

import org.mohamed.testdata.Entity.CartEntity;
import org.springframework.data.repository.CrudRepository;

public interface CartRepository extends CrudRepository<CartEntity, Integer> {
}
