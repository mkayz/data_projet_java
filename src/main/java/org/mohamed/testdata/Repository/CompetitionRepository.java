package org.mohamed.testdata.Repository;

import org.mohamed.testdata.Entity.CompetitionEntity;
import org.springframework.data.repository.CrudRepository;

public interface CompetitionRepository extends CrudRepository<CompetitionEntity, Integer> {
}
