package org.mohamed.testdata.Repository;

import org.mohamed.testdata.Entity.EventEntity;
import org.springframework.data.repository.CrudRepository;

public interface EventRepository extends CrudRepository<EventEntity, Integer> {
}
