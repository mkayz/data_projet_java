package org.mohamed.testdata.Repository;

import org.mohamed.testdata.Entity.ItemEntity;
import org.springframework.data.repository.CrudRepository;

public interface ItemRepository extends CrudRepository<ItemEntity, Integer> {
}
