package org.mohamed.testdata.Repository;

import org.mohamed.testdata.Entity.PaymentEntity;
import org.springframework.data.repository.CrudRepository;

public interface PaymentRepository extends CrudRepository<PaymentEntity, Integer> {
}
