package org.mohamed.testdata.Repository;

import org.mohamed.testdata.Entity.PlayerEntity;
import org.springframework.data.repository.CrudRepository;

public interface PlayerRepository extends CrudRepository<PlayerEntity, Integer> {
}
