package org.mohamed.testdata.Repository;

import org.mohamed.testdata.Entity.ResetPasswordRequestEntity;
import org.springframework.data.repository.CrudRepository;

public interface ResetPasswordRequestRepository extends CrudRepository<ResetPasswordRequestEntity, Integer> {
}
