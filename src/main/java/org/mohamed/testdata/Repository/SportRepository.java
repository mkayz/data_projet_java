package org.mohamed.testdata.Repository;

import org.mohamed.testdata.Entity.SportEntity;
import org.springframework.data.repository.CrudRepository;

public interface SportRepository extends CrudRepository<SportEntity, Integer> {
}
