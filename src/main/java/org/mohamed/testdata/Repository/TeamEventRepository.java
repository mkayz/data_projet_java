package org.mohamed.testdata.Repository;

import org.mohamed.testdata.Entity.TeamEventEntity;
import org.springframework.data.repository.CrudRepository;

public interface TeamEventRepository extends CrudRepository<TeamEventEntity, Integer> {
}
