package org.mohamed.testdata.Repository;

import org.mohamed.testdata.Entity.TeamEntity;
import org.springframework.data.repository.CrudRepository;

public interface TeamRepository extends CrudRepository<TeamEntity, Integer> {
}
