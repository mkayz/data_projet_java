package org.mohamed.testdata.Repository;

import org.mohamed.testdata.Entity.TypeOfBetEntity;
import org.springframework.data.repository.CrudRepository;

public interface TypeOfBetRepository extends CrudRepository<TypeOfBetEntity, Integer> {
}
