package org.mohamed.testdata.Repository;

import org.springframework.data.repository.CrudRepository;

public interface TypeOfPaymentRepository extends CrudRepository<TypeOfPaymentRepository, Integer> {
}
