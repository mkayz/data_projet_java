package org.mohamed.testdata.Repository;

import org.mohamed.testdata.Entity.UserEntity;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<UserEntity, Integer> {

}
