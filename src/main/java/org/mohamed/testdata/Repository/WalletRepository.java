package org.mohamed.testdata.Repository;

import org.mohamed.testdata.Entity.WalletEntity;
import org.springframework.data.repository.CrudRepository;

public interface WalletRepository extends CrudRepository<WalletEntity, Integer> {
}
