package org.mohamed.testdata.Repository;

import org.mohamed.testdata.Entity.WebsiteWalletEntity;
import org.springframework.data.repository.CrudRepository;

public interface WebsiteWalletRepository extends CrudRepository<WebsiteWalletEntity, Integer> {
}
